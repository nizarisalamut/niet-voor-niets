package com.example.davinci.nvn;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.PushService;

import org.w3c.dom.Text;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends Activity {
    private List<Class> myClass = new ArrayList<Class>();
    ArrayList<String> listOfClasses = new ArrayList<String>();
    ArrayList<String> listOfClassId = new ArrayList<String>();
    static boolean appIsRunning;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences myPrefs = this.getSharedPreferences("com.example.app", Context.MODE_PRIVATE);
        int clickedClass = myPrefs.getInt("ClassClicked", -1);
        int WentBack = myPrefs.getInt("WentBack", -1);

        Parse.initialize(this, "QGr7SiC0ROlcAJsSmB4ryzFgviGcNYMPz7JlCvCa", "RGKbdyKiqRaMwiXtYUJYISlRDpDgH7FxsJBCSHF7");

        PushService.setDefaultPushCallback(this, MainActivity.class);

        ParseInstallation.getCurrentInstallation().saveInBackground();

        PushService.startServiceIfRequired(this);

        if ((WentBack == 1)|| (WentBack == -1)) {
        }
        else {
            Intent intent = new Intent(this, MessageListActivity.class);
            startActivity(intent);
            finish();
        }
        getListOfClasses();
    }

    private void getListOfClasses() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Klas").orderByAscending("Klasnaam");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> UserList, ParseException e) {
                if (e == null) {
                    if (UserList.size() > 0) {
                        for (int i = 0; i < UserList.size(); i++) {
                            ParseObject Klas = UserList.get(i);
                            String className = (String) Klas.get("Klasnaam");
                            String classId = Klas.getObjectId();

                            listOfClasses.add(className);
                            listOfClassId.add(classId);
                        }
                        populateClassList();
                    }
                }
            }
        });
    }

    private void populateClassList() {

        SharedPreferences myPrefs = this.getSharedPreferences("com.example.app", Context.MODE_PRIVATE);
        int clickedClass = myPrefs.getInt("ClassClicked", -1);

        for (int i = 0; i < listOfClasses.size(); i++) {

            if (i == clickedClass) {
                myClass.add(new Class(listOfClasses.get(i).toString(), "2014/2015", R.drawable.checkmark, listOfClassId.get(i).toString()));
            } else {
                myClass.add(new Class(listOfClasses.get(i).toString(), "2014/2015", R.drawable.not_checked_class, listOfClassId.get(i).toString()));
            }
        }
        populateListView();
    }

    private void populateListView() {
        ArrayAdapter<Class> adapter = new MyListAdapter();
        ListView list = (ListView) findViewById(R.id.classListView);
        list.setAdapter(adapter);
        registerClickCallback();
    }

    private void registerClickCallback() {

        ListView list = (ListView) findViewById(R.id.classListView);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View viewClicked, final int position, long id) {

                Class clickedClass = myClass.get(position);

                String classClicked = clickedClass.getMake();
                String classId = clickedClass.getCondition();

                saveSharedPreference(classClicked,position, classId);

                setCheckmarkOnClickedClass(classClicked,position);

                subscribeToClickedClass(classId);

                goToMessageList(classClicked);
            }
        });
    }

    public void saveSharedPreference(String classClicked, int position, String classId){

        SharedPreferences myPrefs = MainActivity.this.getSharedPreferences("com.example.app", Context.MODE_PRIVATE);
        SharedPreferences.Editor e = myPrefs.edit();
        e.putInt("ClassClicked", position);
        e.putString("ClassOfUser", classClicked.toString());
        e.putString("ClassId", classId);
        e.putInt("WentBack", 2);
        e.commit();
    }


    public void setCheckmarkOnClickedClass(String classClicked, int position){

        for (int i = 0; i < listOfClasses.size(); i++) {
            myClass.set(i, new Class(listOfClasses.get(i).toString(), "2014/2015", R.drawable.not_checked_class, ""));
        }

        myClass.set(position, new Class(listOfClasses.get(position).toString(), "2014/2015", R.drawable.checkmark, ""));

        String message = "Toegevoegd aan klas " + classClicked;
        Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
    }

    public void subscribeToClickedClass(String classId){

        List<String> subscribedChannels = ParseInstallation.getCurrentInstallation().getList("channels");
        if (subscribedChannels != null) {
            int amountOfSubscribedChannels = subscribedChannels.size();
            for (int i = 0; i < amountOfSubscribedChannels; i++) {
                ParsePush.unsubscribeInBackground(subscribedChannels.get(i));
            }
        }
        ParsePush.subscribeInBackground("class_" + classId);
    }

    public void goToMessageList(String classClicked){
        Bundle basket = new Bundle();
        basket.putString("ClassName", classClicked);
        Intent intent = new Intent(MainActivity.this, MessageListActivity.class);
        intent.putExtras(basket);
        startActivity(intent);
        finish();
    }

    private class MyListAdapter extends ArrayAdapter<Class> {

        public MyListAdapter() {
            super(MainActivity.this, R.layout.item_view, myClass);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemView = convertView;
            if (itemView == null) {
                itemView = getLayoutInflater().inflate(R.layout.item_view, parent, false);
            }
            Class currentClass = myClass.get(position);

            //Icon
            ImageView imageView = (ImageView) itemView.findViewById(R.id.item_icon);
            imageView.setImageResource(currentClass.getIconID());

            //Make
            TextView makeText = (TextView) itemView.findViewById(R.id.item_txtMake);
            makeText.setText(currentClass.getMake());

            //Year
            TextView yearText = (TextView) itemView.findViewById(R.id.item_txtYear);
            yearText.setText("" + currentClass.getYear());

            //Condition
            TextView conditionText = (TextView) itemView.findViewById(R.id.item_txtCondition);
            conditionText.setText(currentClass.getCondition());

            return itemView;
        }
    }
}
