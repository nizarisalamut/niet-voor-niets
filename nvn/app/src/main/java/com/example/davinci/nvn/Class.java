package com.example.davinci.nvn;

/**
 * Created by Kemal on 3-11-2014.
 */
public class Class {
    private String make;
    private String year;
    private int iconID;
    private String condition;

    public Class(String make, String year, int iconID, String condition) {
        super();
        this.make = make;
        this.year = year;
        this.iconID = iconID;
        this.condition = condition;
    }

    public String getMake() {
        return make;
    }

    public String getYear() {
        return year;
    }

    public int getIconID() {
        return iconID;
    }

    public String getCondition() {
        return condition;
    }
}
