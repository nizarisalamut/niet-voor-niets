package com.example.davinci.nvn;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.provider.Settings;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.PushService;
import com.parse.SaveCallback;

/**
 * Created by sheeraz on 10-11-2014.
 */
public class MainApplication extends Application {
    private static MainApplication instance = new MainApplication();

    public MainApplication() {
        instance = this;
    }

    public static Context getContext() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Parse.initialize(this, "QGr7SiC0ROlcAJsSmB4ryzFgviGcNYMPz7JlCvCa", "RGKbdyKiqRaMwiXtYUJYISlRDpDgH7FxsJBCSHF7");
        PushService.setDefaultPushCallback(getApplicationContext(), MainActivity.class);
        final ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        final String  androidId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        // Post the uniqueId delayed
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    installation.put("UniqueId", androidId);
                                    installation.saveInBackground(new SaveCallback() {
                                        @Override
                                        public void done(ParseException e) {
                                            // Saved!
                                        }
                                    });
                                }
                            }, 10000
        );




        //Parse.initialize(this, "CFMyh39aqtSwFf60Y54ihp5VblTUzkSutqhkFegL", "zsn8oAOz3w5WaSjSu42F1JJvtgdlIdBHYPCz7IUq");
/*        Parse.initialize(this, "QGr7SiC0ROlcAJsSmB4ryzFgviGcNYMPz7JlCvCa", "RGKbdyKiqRaMwiXtYUJYISlRDpDgH7FxsJBCSHF7");

        PushService.setDefaultPushCallback(this, MainActivity.class);

        ParseInstallation.getCurrentInstallation().saveInBackground();

        PushService.startServiceIfRequired(this);*/
    }
}
