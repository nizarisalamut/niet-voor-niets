package com.example.davinci.nvn;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

/**
 * Created by Kemal on 3-11-2014.
 */

public class FirstScreen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.firstscreen_layout);

        SharedPreferences myPrefs = this.getSharedPreferences("com.example.app", Context.MODE_PRIVATE);
        int WentBack = myPrefs.getInt("WentBack", -1);

        if (WentBack != -1) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        } else{

            Thread timer = new Thread(){
                public void run(){
                    try{
                        sleep(2000);
                    } catch (InterruptedException e){
                        e.printStackTrace();
                    } finally {
                        Intent openStartingPoint = new Intent("com.example.davinci.nvn.MainActivity");
                        startActivity(openStartingPoint);
                    }
                }
            };
            timer.start();
        }


    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
