package com.example.davinci.nvn;

/**
 * Created by Kemal on 17-11-2014.
 */
public class Message {
    private String message;
    private String date;

    public Message(String message, String date) {
        super();
        this.message = message;
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public String getDate() {
        return date;
    }
}