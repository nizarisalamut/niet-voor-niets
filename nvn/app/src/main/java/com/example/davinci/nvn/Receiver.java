package com.example.davinci.nvn;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.parse.ParsePushBroadcastReceiver;

import java.net.ContentHandler;

/**
 * Created by sheeraz on 3-11-2014.
 */
public class Receiver extends ParsePushBroadcastReceiver{

    public void onPushOpen(Context context, Intent intent){

        if ( MainActivity.appIsRunning == false){
            Intent i = new Intent(context, MainActivity.class);
            i.putExtras(intent.getExtras());
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }
    }

    @Override
    public void onPushReceive(Context context, Intent intent) {
        super.onPushReceive(context, intent);
        if ( MainActivity.appIsRunning == true){
            Intent i = new Intent(context, MessageListActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(i);
        }
    }
}
