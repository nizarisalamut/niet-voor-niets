package com.example.davinci.nvn;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by sheeraz on 17-11-2014.
 */
public class MessageListActivity extends Activity{

    ArrayList<String> listOfMessages = new ArrayList<String>();
    ArrayList<String> listOfDates = new ArrayList<String>();
    String classOfUser;
    String classId;
    MenuItem changeClassMenuItem;
    private List<Message> myMessages = new ArrayList<Message>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_list);

        SharedPreferences myPrefs = this.getSharedPreferences("com.example.app", Context.MODE_PRIVATE);
        String classId = myPrefs.getString("ClassId", null);

        if(classId.equals("")){

            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);
            finish();

        }else {
            this.classId = classId.toString();
        }

        getClassName(this.classId);

        ImageView btn1 = (ImageView)findViewById(R.id.settingsButtonImageView);
        btn1.setOnClickListener(imageViewListener);

        listOfMessages.clear();
        listOfDates.clear();
        myMessages.clear();

        getListOfMessages();
    }


    public void getClassName(String classId) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Klas");
        query.whereEqualTo("objectId", classId);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> ClassList, ParseException e) {
                if (e == null) {
                    if (ClassList.size() > 0) {

                        ParseObject clss = ClassList.get(0);
                        classOfUser  = (String) clss.get("Klasnaam");

                        TextView classNameTextView;
                        classNameTextView = (TextView) findViewById(R.id.classNameTextView);
                        classNameTextView.setText(classOfUser.toString());
                    }
                    else {
                    }
                }
            }
        });
    }


    private View.OnClickListener imageViewListener = new View.OnClickListener()
    {
        public void onClick(View v)
        {
            MessageListActivity.this.openOptionsMenu();
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        changeClassMenuItem = menu.getItem(0);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {

            SharedPreferences myPrefs = MessageListActivity.this.getSharedPreferences("com.example.app", Context.MODE_PRIVATE);
            SharedPreferences.Editor e = myPrefs.edit();
            e.putInt("WentBack", 1);
            e.commit();

            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);
            finish();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void populateMessageList() {

        for (int i = 0; i < listOfMessages.size(); i++) {
            myMessages.add(new com.example.davinci.nvn.Message(listOfMessages.get(i).toString(), listOfDates.get(i).toString()));
        }

        populateMessageView();
    }

    private void populateMessageView() {
        ArrayAdapter<Message> adapter = new MyMessageAdapter();
        ListView list = (ListView) findViewById(R.id.messageListView);
        list.setAdapter(adapter);
    }


    private class MyMessageAdapter extends ArrayAdapter<com.example.davinci.nvn.Message> {

        public MyMessageAdapter() {
            super(MessageListActivity.this, R.layout.message_view, myMessages);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemView = convertView;
            if (itemView == null) {
                itemView = getLayoutInflater().inflate(R.layout.message_view, parent, false);
            }

            com.example.davinci.nvn.Message currentMessage = myMessages.get(position);

            TextView messageText = (TextView) itemView.findViewById(R.id.item_txtMessage);
            messageText.setText(currentMessage.getMessage());

            TextView dateText = (TextView) itemView.findViewById(R.id.item_txtDate);
            dateText.setText(currentMessage.getDate());

            return itemView;
        }
    }

    //LIST OF MESSAGES
    private void getListOfMessages() {

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Push");
        query.orderByDescending("createdAt");
        query.whereEqualTo("KlasId", classId);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> MessageList, ParseException e) {
                if (e == null) {
                    if (MessageList.size() > 0) {
                        for (int i = 0; i < MessageList.size(); i++) {
                            ParseObject msg = MessageList.get(i);
                            String messageName = (String) msg.get("Pushnotification");

                            listOfMessages.add(messageName);

                            String dateParse = msg.getCreatedAt().toString();
                            String replacedDate = dateParse.replace("CET","");
                            String replacedDateGMT = replacedDate.replace("GMT","");

                            listOfDates.add(replacedDateGMT);
                        }
                    }
                    else {
                        listOfMessages.add("Er zijn geen roosterwijzigingen voor deze klas.");
                        listOfDates.add("");
                    }
                    populateMessageList();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        MainActivity.appIsRunning = true;
        getClassName(this.classId);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MainActivity.appIsRunning = false;
    }
}
